import React, {Component} from 'react'
import './BMI.css'

export default class BMI extends Component {
    constructor(props) {
        super(props)
        this.state =  {
             height: 5 * 12, // 5 feet
             weight: 150     // 150 pounds
        }

        this.handleHeightChange = this.handleHeightChange.bind(this);
        this.handleWeightChange = this.handleWeightChange.bind(this);
    }

    handleHeightChange(ev) {
        //console.log("ev.target.value: ", ev.target.value);
        this.setState({height: ev.target.value});
    }

    handleWeightChange(ev) {
        //console.log("ev.target.value: ", ev.target.value);
        this.setState({weight: ev.target.value});
    }

    displayHeight() {
        // Get feet as a integer
        const feet = Math.floor(this.state.height / 12);
        // Get Inches
        const inches = this.state.height % 12;
        // using ` (tick) for string interpolation in JS
        // using ${} to refer a variable value in JS
        return `${feet} feet ${inches} ` + this.pluralize(inches, 'inch', 'inches')
    }

    displayWeight() {
        
        return this.state.weight + ' ' + this.pluralize(this.state.weight, 'pound', 'pounds')
    }

    displayBMI() {
        let bmi =  703 * this.state.weight / (this.state.height * this.state.height);
        return Number.parseFloat(bmi).toPrecision(4);
    }

    displayClassification() {
        // BMI Categories:
        // Underweight = <18.5
        // Normal weight = 18.5–24.9
        // Overweight = 25–29.9
        // Obesity = BMI of 30 or greater

        const bmi = Number.parseFloat(this.displayBMI());
        if (bmi < 18.5) {
            return 'Underweight'
        } else if (bmi < 24.9) {
            return 'Normal'
        } else if (bmi < 29.9) {
            return 'Overweight'
        } else {
            return <div>
                        <span className='warning'>Obese</span>
                        {' '}
                        <a className='siteLink' href="http://www.jennycraig.com.au">
                            What can I do?
                        </a>
                    </div>
        }
    }

    pluralize(count, singular, plural) {
        if(count === 1) {
            return singular
        }
        return plural
    }

    render() {
        return <div className="container">
            <h1>BMI Calculator</h1>
        
            <p>Height</p>
            <p>
                <input type="range" value={this.state.height}
                       min="1" max={8 * 12}
                       onChange={this.handleHeightChange}/>
            </p>
            <p>Weight</p>
            <p>
                <input type="range" value={this.state.weight}
                       min="1" max={350}
                       onChange={this.handleWeightChange}/>
            </p>
            <div className="result">
                {this.displayHeight()}
            </div>
            <div className="result">
                {this.displayWeight()}
            </div>

            <div className="result">
                {this.displayBMI()}
            </div>
            <div className="result">
                {this.displayClassification()}
            </div>
        </div>
    }
}